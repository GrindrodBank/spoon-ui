def label = "spoon-ui-build-${UUID.randomUUID().toString()}"
def mainBranch = "master"

podTemplate(label: label, containers: [
    containerTemplate(name: 'docker', image: 'docker', command: 'cat', ttyEnabled: true),
    containerTemplate(name: 'maven', image: 'maven:3.5.3-jdk-8-alpine', command: 'cat', ttyEnabled: true),
    containerTemplate(name: 'node', image: 'node:10.13.0-stretch', command: 'cat', ttyEnabled: true),
    containerTemplate(name: 'sonar-scanner', image: 'newtmitch/sonar-scanner:3.2.0', command: 'cat', ttyEnabled: true),
],
    volumes: [
        hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock'),
        hostPathVolume(mountPath: '/root/.m2/repository', hostPath: '/root/.m2/repository'),
        hostPathVolume(mountPath: '/root/.npm', hostPath: '/root/.npm'),
        emptyDirVolume(mountPath: '/tmp')
    ]) {

    node(label) {
        def myRepo = checkout scm
        def GIT_COMMIT = "${myRepo.GIT_COMMIT[0..10]}"
        def GIT_BRANCH = myRepo.GIT_BRANCH

        echo "GIT_COMMIT ${GIT_COMMIT}"

        def baseImageName = "grindrodbank/spoon-ui"

        def runImage = "${baseImageName}:${GIT_COMMIT}"

        def didFail = false
        def throwable = null

        echo "NPM Artifactory: $ARTIFACTORY_NPM_LOCAL"
        echo "Docker Artifactory: ${DOCKER_LOCAL_REPO}"
        
        try {
            stage('Setup VERSION Variable') {            
	        	container('docker') {
	        	     sh """
	        		   echo "runImage:: ${runImage}"
	        		 """
	        		 
	        		 script {
	                    VERSION = sh(returnStdout: true, 
	                    script: """
	                        cat package.json | grep version | head -1 | awk -F: '{ print \$2 }' | sed 's/[\",]//g' | tr -d '[[:space:]]'                    
	                    """
	                    ).trim()
	                 } 
	                
	        		 sh """        		   
	        		   echo "VERSION:: ${VERSION}"
	        		   echo "VERSION:: $VERSION"        		   
	        		 """        		             
	        	}
	        }	        
	        
            stage('npm Login to Artifactory') {
                container('docker') {
                    sh "rm -f ~/.npmrc"
                    withCredentials([usernamePassword(credentialsId: 'Jenkins-Artifactory-Credentials', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                        def cmd = "docker run " +
                            "-e NPM_USER=$USERNAME " +
                            "-e NPM_PASS=$PASSWORD " +
                            "-e NPM_EMAIL=mvniekerk@gmail.com " +
                            "-e NPM_REGISTRY=https://${ARTIFACTORY_NPM_LOCAL} " +
                            "-e NPM_SCOPE=@grindrodbank " +
                            "bravissimolabs/generate-npm-authtoken > docker/npmrc"

                        npmrc = sh(
                            script: cmd,
                            returnStdout: true
                        ).trim()

                        sh "echo '//${ARTIFACTORY_NPM_LOCAL}:always-auth=true' >> docker/npmrc"
                        sh "cp docker/npmrc .npmrc"                        
                    }
                }
            }
            
            stage('Build+Unit Test') {
                container('node') {
                    sh "npm config set userconfig docker/npmrc"
                    sh "yarn install --ignore-engines"
                    sh "npm test"   
                    sh "npm run webpack:prod"
                }
            }            

            stage('Build docker image') {
                container('docker') {
                    withDockerRegistry([credentialsId: 'Jenkins-Artifactory-Credentials', url: "https://${DOCKER_LOCAL_REPO}"]) {
                        sh "docker build -t ${runImage} -f Dockerfile ."
                        sh "docker tag ${runImage} ${DOCKER_LOCAL_REPO}${runImage}"
                    }
                }
            }
            
            stage('Publish NPM & Docker image') {
                if (GIT_BRANCH == mainBranch) {
                    container('node') {
                        sh "npm config set userconfig docker/npmrc"
                        sh "npm run lib"
                        sh "npm publish dist/"
                    }
                    
                    container('docker') {                    
                        withDockerRegistry([credentialsId: 'Jenkins-Artifactory-Credentials', url: "https://${DOCKER_LOCAL_REPO}"]) {
                            sh "docker tag ${DOCKER_LOCAL_REPO}${runImage} ${DOCKER_LOCAL_REPO}${baseImageName}:latest"
                            sh "docker tag ${DOCKER_LOCAL_REPO}${runImage} ${DOCKER_LOCAL_REPO}${baseImageName}:${VERSION}"
                            
                            sh "docker push ${DOCKER_LOCAL_REPO}${baseImageName}:${VERSION}"
                            sh "docker push ${DOCKER_LOCAL_REPO}${baseImageName}:latest"
                        }
                	}
                }
            }
           
        } catch (e) {
            didFail = true
            throwable = e
        } finally {
            sh "rm -rf /tmp/${label}"
        }
        if (didFail) {
            echo "Something went wrong. Should be sending a mail"
            error throwable
        }
    }
}