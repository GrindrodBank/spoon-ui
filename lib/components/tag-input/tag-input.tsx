import '../dropdown/dropdown.scss';
import './tag-input.scss';
import React, { ChangeEvent } from 'react';
import { Input, Label } from 'reactstrap';
import { ScrollableArea } from '../scrollable-area/scrollable-area';
import { ITranslatedSelectableValue, translateItem, TranslatedValueOrKey } from '../../util/translation';
import { DropdownTag } from '../dropdown/dropdown-tag';
import { Grid } from '../grid/grid';
import { IDropdownItem } from '../dropdown/dropdown-item';
import { WithPopover } from '../with-popover/with-popover';

export interface ITagInputProps {
  id?: string;
  searching: boolean;
  values: Array<ITranslatedSelectableValue<string>>;
  message?: string;
  placeholder?: TranslatedValueOrKey<string>;
  onAddTag?: (s: ITranslatedSelectableValue<string>) => void;
  onSelectionChanged?: (s: ITranslatedSelectableValue<string>) => void;
  label?: TranslatedValueOrKey<string>;
  disabled?: boolean;
}

export interface ITagInputState {
  selection: Array<ITranslatedSelectableValue<string>>;
  dropdownOpen: boolean;
  search?: string;
}

export class TagInput extends React.Component<ITagInputProps, ITagInputState> {
  static defaultProps: ITagInputProps = {
    searching: false,
    disabled: false,
    values: []
  };

  state: ITagInputState = {
    search: '',
    selection: [],
    dropdownOpen: true
  };

  constructor(props) {
    super(props);
    this.onKeyPressed = this.onKeyPressed.bind(this);
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen,
      search: ''
    }));
  }

  onKeyPressed(val) {
    if (val.keyCode === 13) {
      if (!this.state.search || this.state.search.trim() === '') {
        return;
      }
      const searchText = this.state.search.trim();
      searchText
        .split(',')
        .map(a => a.trim())
        .filter(a => !!a)
        .forEach(search => {
          let item = this.props.values.find(a => a.value.toString().toLowerCase() === search.toLowerCase());
          if (!item) {
            item = {
              display: search,
              value: search,
              selected: true
            };
            !!this.props.onAddTag && this.props.onAddTag(item);
          }
          item.selected = true;
          if (!this.state.selection.find(a => a.value.toString().toLowerCase() === search.toLowerCase())) {
            this.setState(p => {
              const items = [...p.selection, item];
              !!this.props.onSelectionChanged && this.props.onSelectionChanged(item);
              return {
                selection: items,
                search: ''
              };
            });
          } else {
            // this.searchItem.next('');
            this.setState(_ => ({
              search: ''
            }));
          }
        });
    } else if (val.keyCode === 27) {
      this.setState(_ => ({ search: '' }));
    }
  }

  private valSelected = (val: ITranslatedSelectableValue<string>) => {
    this.setState(prevState => {
      const vals = prevState.selection;
      if (!vals.includes(val)) {
        vals.push(val);
      }
      return { selection: vals };
    });
  };

  private valDeselected = (val: ITranslatedSelectableValue<string>) => {
    val.selected = false;
    this.setState(prevState => {
      const vals = prevState.selection;
      if (vals.includes(val)) {
        vals.splice(vals.indexOf(val), 1);
      }
      return { selection: vals };
    });
  };

  private onSearchChanged = (val: ChangeEvent<HTMLInputElement>) => {
    if (val.target) {
      const value = val.target.value;
      // this.searchItem.next(value);
      this.setState(_ => ({ search: value }));
    }
  };

  private renderTag = (item: IDropdownItem<string>) => (
    <DropdownTag
      selectable
      value={item}
      key={item.name || (item.value && item.value.toString())}
      onSelected={this.valSelected}
      onDeselected={this.valDeselected}
    />
  );

  private renderSelectionBarItem = v => (
    <DropdownTag
      value={v}
      disabled={this.props.disabled}
      key={v.name || (v.value && v.value.toString())}
      onSelected={this.valSelected}
      onDeselected={this.valDeselected}
    />
  );

  private renderSelectionBar = selectedItems => (
    <Grid className="tags-container selected-tags" items={selectedItems} itemRender={this.renderSelectionBarItem} />
  );

  render() {
    const { label, placeholder, message, id } = this.props;
    const items =
      !this.props.searching && !!this.props.values
        ? this.props.values.filter(
            b =>
              !this.state.search ||
              !this.state.search.trim() ||
              ('' + b.value).toLowerCase().indexOf(this.state.search.toLowerCase()) >= 0 ||
              translateItem(b)
                .toLowerCase()
                .indexOf(this.state.search.toLowerCase()) >= 0
          )
        : [];
    const selectedItems = this.props.values.filter(b => b.selected);
    const onHandlePopover = items.length === 0 ? () => this.onKeyPressed({ keyCode: 13 }) : null;

    return (
      <>
        {label && <Label>{translateItem(label)}</Label>}
        <WithPopover
          id={id}
          className={`tag-input ${items.length === 0 ? 'empty-popover' : ''} ${selectedItems.length === 0 ? 'empty-selected' : ''}`}
          autoOpen
          autoClose
          closeOnMainClick
          disabled={this.props.disabled}
          onClosePopover={onHandlePopover}
          onOpenPopover={onHandlePopover}
          mainComponent={
            <div>
              {!!message && <Label for="inputwithoutvalidation">{message}</Label>}
              <Input
                id={`${id}-input`}
                disabled={this.props.disabled}
                placeholder={translateItem(placeholder)}
                value={this.state.search}
                onChange={this.onSearchChanged}
                onKeyDown={this.onKeyPressed}
              />
            </div>
          }
        >
          <div className="tags multiple">
            <ScrollableArea>
              {this.props.searching ? (
                <i key="searching">Searching</i>
              ) : (
                <Grid className="tags-container" items={items} itemRender={this.renderTag} />
              )}
            </ScrollableArea>
          </div>
        </WithPopover>
        {this.renderSelectionBar(selectedItems)}
      </>
    );
  }
}
