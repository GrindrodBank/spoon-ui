import React, { ReactNode, HTMLAttributes } from 'react';
import { Table as RTable, Card, TableProps } from 'reactstrap';
import { TableHeader } from './table-header';
import { Row, Col } from '../layout';
import './table.scss';

interface ITableProps extends TableProps {
  tableHeaderData?: any[];
  tableRowData: any[];
  renderRow?: (arg: any) => ReactNode;
  renderHeader?: (arg: any) => ReactNode;
}

const defaultHeaderRender = (tableHeader: any) => (
  <TableHeader key={tableHeader.id ? tableHeader.id : tableHeader}>{tableHeader}</TableHeader>
);
const defaultRowRender = (rowData: any[]) => (
  <tr>
    {' '}
    {rowData.map(item => (
      <td key={item.id ? item.id : item}>{item}</td>
    ))}{' '}
  </tr>
);

export class Table extends React.Component<ITableProps> {
  static defaultProps = {
    renderRow: defaultRowRender,
    renderHeader: defaultHeaderRender,
    tableHeaderData: []
  };

  renderTableHeaders = (tableHeaderData, renderHeader) =>
    tableHeaderData.length > 0 ? <tr> {tableHeaderData.map(renderHeader)}</tr> : renderHeader();

  renderTableRows = (tableRowData, renderRow) => tableRowData.map(renderRow);

  render() {
    const { tableHeaderData, tableRowData, renderRow, renderHeader, ...other } = this.props;
    return (
      <RTable {...other}>
        <thead>{this.renderTableHeaders(tableHeaderData, renderHeader)}</thead>
        <tbody>{this.renderTableRows(tableRowData, renderRow)}</tbody>
      </RTable>
    );
  }
}
