import './footer.scss';

import React from 'react';
import { Col, Row } from '../layout';
import { translateItem } from '../../util';

export const Footer = ({ projectName, version, versionApi = null }) => {
  const text = `${projectName} ${translateItem('copyright')} © ${new Date().getFullYear()} Grindrod Bank. ${translateItem(
    'rights-reserved'
  )}.`;
  return (
    <footer>
      <Row className="gap">
        <Col md="12" justify="end" />
      </Row>
      <hr />
      <Row className="text">
        <Col md="12" justify="end" align="center">
          {text}
          &nbsp; <span className="version">v{version}</span>
          {versionApi && (
            <>
              &nbsp;
              {translateItem('usingAPI')}
              &nbsp; <span className="version">{versionApi}</span>
            </>
          )}
        </Col>
      </Row>
    </footer>
  );
};
