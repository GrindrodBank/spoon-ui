import React from 'react';
import { Row, Col } from '../layout';
import { Button } from '../button/button';
import { translateItem } from '../../util/translation';
import './login.scss';

export interface ILoginFormProps {
  title: string;
  logoUrl: string;
  titleUrl: string;
  onLoginClicked: () => void;
}
interface ILoginFormState {
  username: string;
  password: string;
}

export class LoginForm extends React.Component<ILoginFormProps, ILoginFormState> {
  render() {
    return (
      <Row>
        <Col justify="center">
          <div className="login-card">
            <Row>
              <Col justify="center">
                <img className="logo-grindrod-bank" src="content/images/Grindrod_Bank_logo_logotype.svg" alt="Logo" />
              </Col>
            </Row>
            <Row justify="center">
              <div>
                <img src={this.props.logoUrl} alt="Logo" />
              </div>
            </Row>
            <Row>
              <Col justify="center" className="powered-by">
                <span> {translateItem('login.form.label')}</span>
                <img src={this.props.titleUrl} />
              </Col>
            </Row>
            <Row>
              <Col className="d-flex justify-content-center">
                <Button color="primary" className="button-login" onClick={this.props.onLoginClicked}>
                  {translateItem('login.form.button')}
                </Button>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    );
  }
}
