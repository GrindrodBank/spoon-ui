import React, { ReactNode } from 'react';
import { CardBody, Card as RCard, CardTitle, CardSubtitle, CardProps } from 'reactstrap';
import cx from 'classnames';
import './card2.scss';
import { Row } from '../layout';

export interface ICard2Props extends CardProps {
  image: ReactNode;
  title: string;
  subtitle: string;
  actionComponent: ReactNode;
}

export class Card2 extends React.Component<ICard2Props> {
  render() {
    const { image, title, subtitle, actionComponent, className, ...other } = this.props;
    return (
      <RCard className={cx('card2', 'base-card', className)} {...other}>
        <CardBody>
          <div className="card-image">{image}</div>
          <CardTitle>
            <h3>{title}</h3>
          </CardTitle>
          <CardSubtitle>
            <p>{subtitle}</p>
          </CardSubtitle>
          <Row justify="center">{actionComponent}</Row>
        </CardBody>
      </RCard>
    );
  }
}
