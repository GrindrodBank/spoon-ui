import React from 'react';
import { Button } from './button';
import { Row } from 'reactstrap';
import { ITranslatedValue, translateItem } from '../../util/translation';

export interface IToggleButtonGroupProps<T> {
  values: Array<ITranslatedValue<T>>;
  unselectable?: boolean;
  selectedDefaultIndex?: number;
  onChanged?: (value: any) => void;
}

export interface IToggleButtonGroupState {
  toggleId: number;
}

export class ToggleButtonGroup<T> extends React.Component<IToggleButtonGroupProps<T>, IToggleButtonGroupState> {
  static defaultProps: IToggleButtonGroupProps<any> = {
    selectedDefaultIndex: null,
    values: []
  };

  state: IToggleButtonGroupState = {
    toggleId: undefined
  };

  constructor(props: any) {
    super(props);
    this.state = {
      toggleId: !isNaN(this.props.selectedDefaultIndex) ? this.props.selectedDefaultIndex : null
    };
  }

  onBtnClick = selected => {
    const { toggleId } = this.state;
    const { unselectable } = this.props;
    this.setState({ toggleId: unselectable ? (toggleId === selected ? null : selected) : selected });
    if (!!this.props.onChanged) {
      this.props.onChanged(selected);
    }
  };

  render() {
    return (
      <Row>
        {this.props.values.map((v, index) => (
          // tslint:disable:jsx-no-lambda
          <Button className="toggle-btn" key={index} active={this.state.toggleId === index} onClick={() => this.onBtnClick(index)}>
            {translateItem(v.value)}
          </Button>
        ))}
      </Row>
    );
  }
}
