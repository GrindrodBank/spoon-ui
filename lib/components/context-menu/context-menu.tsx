import '../context-menu/context-menu.scss';
import React, { HTMLAttributes, ReactElement } from 'react';
import cx from 'classnames';
import { ISimpleMenuItem, SimpleMenu } from '../simple-menu/simple-menu';
import { WithPopover } from '../with-popover/with-popover';

export interface IContextMenuProps extends HTMLAttributes<HTMLDivElement> {
  items: ISimpleMenuItem[];
  mainComponent?: ReactElement;
}

export class ContextMenu extends React.Component<IContextMenuProps> {
  render() {
    const { className, items, mainComponent } = this.props;
    const menuClassname = cx('context-menu', className);
    return (
      <WithPopover className={menuClassname} noOpener autoOpen autoClose closeOnMainClick mainComponent={mainComponent}>
        <SimpleMenu items={items} />
      </WithPopover>
    );
  }
}
