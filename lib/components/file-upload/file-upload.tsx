import React from 'react';
import { FilePond, FilePondProps, File } from 'react-filepond';
import cx from 'classnames';
import ArrowDropUp from '@material-ui/icons/ArrowDropUpRounded';
import { translateItem, TranslatedValueOrKey } from '../../util/translation';
import * as remove from '../../../static/images/add.svg';
import 'filepond/dist/filepond.min.css';
import './file-upload.scss';
import { Label } from '../layout';
import { FormHelp } from '../form-feedback/form-feedback';

interface IFileUploadProps extends FilePondProps {
  onFileUpdated?: (fileItems: File[]) => void;
  onFileRemoved?: (file: File) => void;
  onError?: (error: { main: string; sub: string }, file?: File, status?: any) => void;
  onFileAdded?: (error: { main: string; sub: string }, file: File) => void;
  disabled?: boolean;
  label?: TranslatedValueOrKey<string>;
  helpMessage?: string;
  large?: boolean;
}

interface IFileUploadState {
  files: File[];
}

export class FileUpload extends React.Component<IFileUploadProps, IFileUploadState> {
  state: IFileUploadState = {
    files: []
  };

  private instance = null;

  private onFileUpdated = (fileItems: File[]) => {
    this.props.onFileUpdated && this.props.onFileUpdated(fileItems);

    this.setState({
      files: fileItems
    });
  };
  render() {
    const fileClassName = cx('upload-file', {
      empty: this.state.files.length === 0,
      large: this.props.large
    });

    return (
      <div className={fileClassName}>
        {this.props.label && <Label>{translateItem(this.props.label)}</Label>}
        {this.props.required && (
          <div className={`required-check${this.props.label ? ' with-label' : ''} `}>
            <ArrowDropUp />
          </div>
        )}
        <FilePond
          files={this.state.files}
          {...this.props}
          // iconRemove='../../../static/images/add.svg' //static/images/add.svg
          labelIdle={this.props.labelIdle ? translateItem(this.props.labelIdle) : ''}
          onupdatefiles={this.onFileUpdated}
          onremovefile={this.props.onFileRemoved}
          onaddfile={this.props.onFileAdded}
          onerror={this.props.onError}
        />
        {!!this.props.helpMessage && <FormHelp helpMessage={this.props.helpMessage} />}
      </div>
    );
  }
}
