import { IButtonProps } from '../components/button/button';

export class CurrentRouteAndIcon {
  id?: string;
  name: string;
  icon: JSX.Element;
  path: string;
  sideMenu?: boolean;
  header?: string;
  breadCrumb?: CurrentRouteAndIcon[];
  actions?: IButtonProps[];
}
